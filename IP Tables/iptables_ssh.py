import spur
import os
from flask import Flask, render_template, redirect, request, url_for

# random generated key
secret_key = os.urandom(24)
app = Flask(__name__)
# Secret key wordt hier toegewezen aan de applicatie
app.secret_key = secret_key

hostname = '13.80.74.83'
username = 'se2'
password = '5#5lGHX&ntdQcU'


@app.route('/', methods=['GET'])
def index():
    shell = spur.SshShell(hostname=hostname, username=username,
                          password=password)
    input_chain = []
    output_chain = []
    forward_chain = []
    input_chain = [x.strip(' ') for x in input_chain]
    output_chain = [x.strip(' ') for x in output_chain]
    forward_chain = [x.strip(' ') for x in forward_chain]
    with shell:
        get_input_chain = shell.run(
            ["sudo", "iptables", "-L", "INPUT", "--line-numbers"])

        result_input = get_input_chain.output
        new_result = result_input.split(b'\n')
        for result in new_result:
            if result == b'':
                continue
            else:
                encoded = result.decode("utf-8")
                input_chain.append(encoded)
        get_output_chain = shell.run(
            ["sudo", "iptables", "-L", "OUTPUT", "--line-numbers"])
        result_output = get_output_chain.output
        new_result = result_output.split(b'\n')
        for result in new_result:
            if result == b'':
                continue
            else:
                encoded = result.decode("utf-8")
                output_chain.append(encoded)

        get_forward_chain = shell.run(
            ["sudo", "iptables", "-L", "FORWARD", "--line-numbers"])
        result_forward = get_forward_chain.output
        new_result = result_forward.split(b'\n')
        for result in new_result:
            if result == b'':
                continue
            else:
                encoded = result.decode("utf-8")
                forward_chain.append(encoded)
    return render_template('index.html', input_chain=input_chain,output_chain=output_chain,
                           forward_chain=forward_chain)


@app.route('/add_input', methods=['POST'])
def add_input():
    shell = spur.SshShell(hostname=hostname, username=username,
                          password=password)
    with shell:
        chain = request.form['chain']
        protocol = request.form['protocol']
        port = request.form['port']
        policy = request.form['policy']
        shell.run(["sudo", "iptables", "-A", chain, "-p", protocol, "--dport", port, "-j",
                   policy])
    return redirect(url_for('index'))


@app.route('/ip_input', methods=['POST'])
def ip_input():
    shell = spur.SshShell(hostname=hostname, username=username,
                          password=password)
    with shell:
        ip = request.form['ip']
        chain = request.form['chain']
        policy = request.form['policy']
        shell.run(["sudo", "iptables", "-A", chain, "-s", ip, "-j", policy])
    return redirect(url_for('index'))


@app.route('/delete_input', methods=['POST'])
def delete_input():
    shell = spur.SshShell(hostname=hostname, username=username,
                          password=password)
    with shell:
        row_number = request.form['remove']
        shell.run(["sudo", "iptables", "-D", "INPUT", row_number])
    return redirect(url_for('index'))


@app.route('/delete_output', methods=['POST'])
def delete_output():
    shell = spur.SshShell(hostname=hostname, username=username,
                          password=password)
    with shell:
        row_number = request.form['remove']
        shell.run(["sudo", "iptables", "-D", "OUTPUT", row_number])
    return redirect(url_for('index'))


@app.route('/delete_forward', methods=['POST'])
def delete_forward():
    shell = spur.SshShell(hostname=hostname, username=username,
                          password=password)
    with shell:
        row_number = request.form['remove']
        shell.run(["sudo", "iptables", "-D", "FORWARD", row_number])
    return redirect(url_for('index'))


@app.route('/change_policy', methods=['POST'])
def change_policy():
    shell = spur.SshShell(hostname=hostname, username=username,
                          password=password)
    with shell:
        chain = request.form['chain']
        policy = request.form['policy']
        shell.run(["sudo", "iptables", "--policy", chain, policy])
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(debug=True)
