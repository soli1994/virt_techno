from api_app.models import Cart
from api_app.config.db import session
from flask_restful import reqparse, abort, Resource, fields, marshal_with
from api_app.models import User
from api_app.models import Products

cart_fields = {
    'id': fields.Integer,
    'email': fields.String,
    'product_name': fields.String,
    'quantity': fields.Integer,
    'price': fields.Integer
}

parser = reqparse.RequestParser()
parser.add_argument('id', type=int)
parser.add_argument('email', type=str)
parser.add_argument('password', type=str)
parser.add_argument('product_name', type=str)
parser.add_argument('quantity', type=int)
parser.add_argument('price', type=int)


class CartView(Resource):
    @marshal_with(cart_fields)
    def get(self):
        parsed_args = parser.parse_args()
        email = parsed_args['email']
        password = parsed_args['password']
        user = session.query(User).filter(
            User.email == email).first()
        if not user.password == password:
            abort(403, message="Credentials are invalid, please try again")
        cart = session.query(Cart).filter(Cart.email == email).all()
        return cart


class CartCreate(Resource):
    @marshal_with(cart_fields)
    def post(self):
        parsed_args = parser.parse_args()
        email = parsed_args['email']
        password = parsed_args['password']
        product_name = parsed_args['product_name']
        user = session.query(User).filter(
            User.email == email).first()
        prod = session.query(Products).filter(
            Products.name == product_name).first()
        if not user.password == password:
            abort(403, message="Credentials are invalid, please try again")
        elif not prod:
            abort(403, message="This product does not exist")
        check_cart = session.query(Cart).filter(
            Cart.product_name == product_name).all()
        if check_cart:
            abort(403, message="This product is already in the basket")
        else:
            create_cart = Cart(email=email, product_name=product_name,
                               quantity=parsed_args['quantity'], price=prod.price)
            session.add(create_cart)
            session.commit()
            return create_cart, 201


class CartEdit(Resource):
    @marshal_with(cart_fields)
    def put(self):
        parsed_args = parser.parse_args()
        email = parsed_args['email']
        password = parsed_args['password']
        id = parsed_args['id']
        user = session.query(User).filter(
            User.email == email).first()
        if not user.password == password:
            abort(403, message="Credentials are invalid, please try again")
        cart = session.query(Cart).filter(Cart.id == id).first()
        cart.quantity = parsed_args['quantity']
        session.add(cart)
        session.commit()
        return cart, 201


class CartDelete(Resource):
    def delete(self):
        parsed_args = parser.parse_args()
        email = parsed_args['email']
        password = parsed_args['password']
        id = parsed_args['id']
        user = session.query(User).filter(
            User.email == email).first()
        if not user.password == password:
            abort(403, message="Credentials are invalid, please try again")
        cart = session.query(Cart).filter(Cart.id == id).first()
        if not cart:
            abort(404, message="Product with id {0} doesn't exist in this cart".format(id))
        session.delete(cart)
        session.commit()
        return 'Product is deleted successfully'
