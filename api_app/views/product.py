from api_app.models import Products
from api_app.config.db import session
from flask_restful import reqparse, abort, Resource, fields, marshal_with

product_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'inventory_level': fields.Integer,
    'price': fields.Integer,
}

parser = reqparse.RequestParser()
parser.add_argument('id',type=int)
parser.add_argument('name', type=str)
parser.add_argument('description', type=str)
parser.add_argument('inventory_level', type=int)
parser.add_argument('price', type=int)


class ProductView(Resource):
    @marshal_with(product_fields)
    def get(self, id):
        product = session.query(Products).filter(Products.id == id).first()
        if not product:
            abort(404, message="Product {} doesn't exist".format(id))
        return product


class ProductsAll(Resource):
    @marshal_with(product_fields)
    def get(self):
        products = session.query(Products).all()
        return products


class ProductCreate(Resource):
    @marshal_with(product_fields)
    def post(self):
        parsed_args = parser.parse_args()
        product = Products(name=parsed_args['name'],
                           description=parsed_args['description'],
                           inventory_level=parsed_args['inventory_level'],
                           price=parsed_args['price'])
        session.add(product)
        session.commit()
        return product, 201


class ProductEdit(Resource):
    @marshal_with(product_fields)
    def put(self):
        parsed_args = parser.parse_args()
        id = parsed_args['id']
        product = session.query(Products).filter(Products.id == id).first()
        if not product:
            abort(404, message="Product with id {0} doesn't exist".format(id))
        product.name = parsed_args['name']
        product.description = parsed_args['description']
        product.inventory_level = parsed_args['inventory_level']
        product.price = parsed_args['price']
        session.add(product)
        session.commit()
        return product, 201


class ProductDelete(Resource):
    def delete(self):
        parsed_args = parser.parse_args()
        id = parsed_args['id']
        product = session.query(Products).filter(Products.id == id).first()
        if not product:
            abort(404, message="Product with id {0} doesn't exist".format(id))
        session.delete(product)
        session.commit()
        return 'Product is deleted successfully'

