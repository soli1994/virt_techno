from api_app.models import Cart
from api_app.config.db import session
from flask_restful import reqparse, abort, Resource, fields, marshal_with
from api_app.models import Order
from api_app.models import User
from datetime import datetime
import requests
import json
cart_fields = {
    'id': fields.Integer,
    'email': fields.String,
    'order_date': fields.DateTime,
    'total_price': fields.Integer,
    'is_paid': fields.Boolean,
    'is_shipped': fields.Boolean
}


parser = reqparse.RequestParser()
parser.add_argument('id', type=int)
parser.add_argument('email', type=str)
parser.add_argument('order_date', type=str)
parser.add_argument('total_price', type=int)
parser.add_argument('is_paid', type=bool)
parser.add_argument('is_shipped', type=bool)
parser.add_argument('password', type=str)


class OrderView(Resource):
    @marshal_with(cart_fields)
    def get(self):
        parsed_args = parser.parse_args()
        email = parsed_args['email']
        password = parsed_args['password']
        user = session.query(User).filter(
            User.email == email).first()
        if not user.password == password:
            abort(403, message="Credentials are invalid, please try again")
        order = session.query(Order).filter(Cart.email == email).all()
        return order


class OrderCreate(Resource):
    @marshal_with(cart_fields)
    def post(self):
        parsed_args = parser.parse_args()
        email = parsed_args['email']
        password = parsed_args['password']
        user = session.query(User).filter(
            User.email == email).first()
        if not user.password == password:
            abort(403, message="Credentials are invalid, please try again")
        url = 'http://localhost:5000/cart?email={0}&password={1}'.format(
            str(email), str(password))
        cart_content = requests.get(url)
        cart_text = cart_content.text
        cart_json = json.loads(cart_text)
        total_price = 0
        for product in cart_json:
            product_price = product['quantity']*product['price']
            total_price += product_price
        order = Order(email=email, order_date=datetime.today(),
                    is_paid=True,is_shipped=False,total_price=total_price)
        user.active = True
        session.add(order)
        session.commit()
        session.query(Cart).filter(Cart.email == email).delete()
        return order
