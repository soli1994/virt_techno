from api_app.models import User
from api_app.config.db import session
from flask_restful import reqparse, abort, Resource, fields, marshal_with

user_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'surname': fields.String,
    'email': fields.String,
    'active': fields.Boolean,
}

parser = reqparse.RequestParser()
parser.add_argument('name', type=str)
parser.add_argument('surname', type=str)
parser.add_argument('email', type=str)
parser.add_argument('password', type=str)


class UserView(Resource):
    @marshal_with(user_fields)
    def get(self):
        parsed_args = parser.parse_args()
        email = parsed_args['email']
        password = parsed_args['password']
        user = session.query(User).filter(User.email == email).first()
        if not user.password == password:
            abort(403, message="Credentials are invalid, please try again")
        return user


class UserDelete(Resource):
    @marshal_with(user_fields)
    def delete(self):
        parsed_args = parser.parse_args()
        email = parsed_args['email']
        password = parsed_args['password']
        user = session.query(User).filter(User.email == email).first()
        if not user.password == password:
            abort(403, message="Credentials are invalid, please try again")
        user = session.query(User).filter(User.email == email).first()
        session.delete(user)
        session.commit()
        return 'Account is deleted succesfully', 204


class UserEdit(Resource):
    @marshal_with(user_fields)
    def put(self):
        parsed_args = parser.parse_args()
        email = parsed_args['email']
        password = parsed_args['password']
        user = session.query(User).filter( User.email == email).first()
        if not user.password == password:
            abort(403, message="Credentials are invalid, please try again")
        user = session.query(User).filter(User.email == email).first()
        user.name = parsed_args['name']
        user.surname = parsed_args['surname']
        session.add(user)
        session.commit()
        return user, 201


class UserCreate(Resource):
    def post(self):
        parsed_args = parser.parse_args()
        user = User(name=parsed_args['name'],surname=parsed_args['surname'],
                    email=parsed_args['email'],password=parsed_args['password'])
        session.add(user)
        session.commit()
        return 201
