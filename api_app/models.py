from sqlalchemy import Column, Integer, String, Boolean,NUMERIC,DATETIME
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import PasswordType
Base = declarative_base()


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    surname = Column(String, nullable=False)
    email = Column(String(120), nullable=False,unique=True)
    password = Column(PasswordType(
        schemes=[
            'pbkdf2_sha512',
            'md5_crypt'
        ],
        deprecated=['md5_crypt']
    ))

    active = Column(Boolean, nullable=False, default=False)

    def __init__(self, name, surname,email,password):
        self.name = name
        self.surname = surname
        self.email = email
        self.password = password


class Products(Base):
    __tablename__ = 'product'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    description = Column(String)
    inventory_level = Column(Integer)
    price = Column(NUMERIC, nullable=False, default=False)

    def __init__(self, name, description, inventory_level, price):
        self.name = name
        self.description = description
        self.inventory_level = inventory_level
        self.price = price


class Cart(Base):
    __tablename__ = 'cart'

    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False)
    product_name = Column(String, nullable=False)
    quantity = Column(Integer,nullable=False)
    price = Column(NUMERIC, nullable=False)

    def __init__(self, email, product_name, quantity, price):
        self.email = email
        self.product_name = product_name
        self.quantity = quantity
        self.price = price


class Order(Base):
    __tablename__ = 'order'

    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False)
    order_date = Column(DATETIME)
    total_price = Column(NUMERIC, nullable=False)
    is_paid = Column(Boolean, nullable=False, default=False)
    is_shipped = Column(Boolean, nullable=False, default=False)


if __name__ == "__main__":
    from sqlalchemy import create_engine
    from api_app.config.settings import DB_URI
    engine = create_engine(DB_URI)
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
