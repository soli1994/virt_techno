from flask import Flask
from flask_restful import Api
from api_app.views.user import UserView, UserCreate, UserEdit,UserDelete
from api_app.views.product import ProductView, ProductsAll,ProductEdit,ProductDelete,\
    ProductCreate
from api_app.views.cart import CartView,CartCreate,CartEdit,CartDelete
from api_app.views.order import OrderCreate,OrderView

app = Flask(__name__)
api = Api(app)


api.add_resource(UserView, '/user', endpoint='user')
api.add_resource(UserCreate, '/user/create', endpoint='user/create')
api.add_resource(UserEdit, '/user/edit', endpoint='user/edit')
api.add_resource(UserDelete, '/user/delete', endpoint='user/delete')

api.add_resource(ProductsAll, '/product', endpoint='products')
api.add_resource(ProductCreate, '/product/create', endpoint='product_create')
api.add_resource(ProductView, '/product/<string:id>', endpoint='product_view')
api.add_resource(ProductEdit, '/product/edit/', endpoint='product_edit')
api.add_resource(ProductDelete, '/product/delete/', endpoint='product_delete')

api.add_resource(CartView, '/cart', endpoint='cart')
api.add_resource(CartCreate, '/cart/create',endpoint='cart_create')
api.add_resource(CartEdit, '/cart/edit',endpoint='cart_edit')
api.add_resource(CartDelete, '/cart/delete', endpoint='cart_delete')

api.add_resource(OrderCreate, '/order/create', endpoint='order_create')
api.add_resource(OrderView, '/order/view', endpoint='order_view')

if __name__ == '__main__':
    app.run(debug=True)
