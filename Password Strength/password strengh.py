import re


def pword(password):
    a = '[A-Z]'

    b = '[|\"|\'|~|!|@|#|$|%|^|&|*|(|)|_|=|+|\||,|.|/|?|:|;|[|]|{\}|<|>]'

    c = '\d+'

    d = '[a-z]'

    with open('10-million-password-list-top-1000000.txt', 'r') as f_obj:
        passwords = f_obj.read()

    if password in passwords:
        print("Your password is frequently used, try again.")
        return False

    if re.search(a, password) and not re.search(b, password) \
            and not re.search(c, password) and not re.search(d, password) \
            or re.search(b, password) and not re.search(a, password) \
            and not re.search(c, password) \
            and not re.search(d, password) or re.search(c, password) \
            and not re.search(a, password) and not re.search(b, password) \
            and not re.search(d, password) or re.search(d, password) \
            and not re.search(a, password) and not re.search(b, password) \
            and not re.search(c, password):
        print("Your password is weak!")
    elif re.search(a, password) and re.search(b, password) \
            and not re.search(c, password) \
            and not re.search(d, password) or re.search(a, password) \
            and re.search(c, password) and not re.search(b, password) \
            and not re.search(d, password) or re.search(a, password) \
            and re.search(d, password) and not re.search(c, password) \
            and not re.search(b, password) or re.search(b, password) \
            and re.search(c, password) and not re.search(a, password) \
            and not re.search(d, password) or re.search(b, password) \
            and re.search(d, password) and not re.search(a, password) \
            and not re.search(c, password) or re.search(c, password) \
            and re.search(d, password) and not re.search(a, password) \
            and not re.search(b, password):
        print("Your password is medium !")
    elif re.search(a, password) and re.search(b, password) \
            and re.search(c, password) \
            and not re.search(d, password) or re.search(a, password) \
            and re.search(c, password) and re.search(d, password) \
            and not re.search(b, password) or re.search(a, password) \
            and re.search(b, password) and re.search(d, password) \
            and not re.search(c, password) or re.search(b, password) \
            and re.search(c, password) and re.search(d, password) \
            and not re.search(a, password):
        print("Your password is strong !")
    elif re.search(a, password) and re.search(b, password) \
            and re.search(c, password) and re.search(d, password):
        print("Your password is very Strong!")

    if len(password) <= 10:
        print("Your password must be 10 characters long.")
        return False

    elif not re.findall(c, password):
        print("You need a number in your password.")
        return False

    elif not re.findall(a, password):
        print("You need a capital letter in your password.")
        return False

    elif not re.findall(b, password):
        print("You need a symbol in your password.")
        return False

    else:
        print("Password Accepted")
        return True


passwordValid = False
while not passwordValid:
    password = input("Type in your password: ")
    passwordValid = pword(password)
